### Disclaimer 

Please make sure the module is meant to work with Cyberpunk RED and is listed on the Manage Modules screen in game. If not, please contact the module creator instead.

### Name of module

[Type text here]

### Versions and builds

- Foundry VTT: [For example: Version 6 Build 153]
- Cyberpunk RED: [For example: v0.56.8]
- Module: [For example: v1.12.8]

### Module purpose

[Type text here]

### Description of issue/request

[Type text here]

### Additional info

[Type text here]

/label ~"Module"
