### Description of bug

[Type text here]

### Versions and builds

- Foundry VTT: [For example: Version 6 Build 153]
- Cyberpunk RED: [For example: v0.56.8]

### Have you been able to reproduce the issue?

[Yes/No]

### Steps to Reproduce

1. [Step 1]
2. [Step 2]
3. [Step 3]

### Expected result

[Type text here]

### Actual result

[Type text here]

### Additional info

[Type text here]

/label ~"Bug::Unconfirmed"
